<?php   
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Debit extends Model
{
    protected $table = "debit";

    // protected $fillable = [];

    // public $timestamps = false;

    public function scopeGetAll($query, $limit)
    {
        return $this
        ->select(
            'debit.id',
            'debit.kode',
            'debit.nama',
            'debit.keterangan',
            'debit.pemilik',
            'debit.status',
            'debit.created_at',
            'debit.updated_at',
            'users.name as nama_admin',
            'pdam.nama as nama_alat',
            (DB::raw('(select ph from debit_monitoring where debit_id=debit.id order by id desc limit 1) as ph ')),
            (DB::raw('(select tds from debit_monitoring where debit_id=debit.id order by id desc limit 1) as tds ')),
            (DB::raw('(select watter_lower from debit_monitoring where debit_id=debit.id order by id desc limit 1) as watter_lower '))
        )
        ->join('pdam', 'pdam.id', '=', 'debit.pdam_id')
        ->join('users', 'users.id', '=', 'debit.user_id')
        ->orderBy('id', 'desc')
        ->paginate(10);
    }

    public function scopeGetAllNoLimit($query)
    {
        return $this
        ->select(
            'debit.id',
            'debit.kode',
            'debit.nama',
            'debit.keterangan',
            'debit.pemilik',
            'debit.status',
            'debit.created_at',
            'debit.updated_at',
            'users.name as nama_admin',
            'pdam.nama as nama_alat',
            (DB::raw('(select ph from debit_monitoring where debit_id=debit.id order by id desc limit 1) as ph ')),
            (DB::raw('(select tds from debit_monitoring where debit_id=debit.id order by id desc limit 1) as tds ')),
            (DB::raw('(select watter_lower from debit_monitoring where debit_id=debit.id order by id desc limit 1) as watter_lower '))
        )
        ->join('pdam', 'pdam.id', '=', 'debit.pdam_id')
        ->join('users', 'users.id', '=', 'debit.user_id')
        ->orderBy('id', 'desc')
        ->get();
    }
}