<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pdam;
use Auth;

class PdamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Pdam::GetAll(5);
        return view('pdam.index', ['data' => $data]);
    }

    public function create()
    {
        return view('pdam.form');
    }

    public function edit($id)
    {
        $data = Pdam::where(['id' => $id])->first();
        return view('pdam.form', ['data' => $data]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'kode' => 'required|min:4|max:4',
            'nama' => 'required|min:0|max:150',
            'keterangan' => 'min:0|max:255',
            'status' => 'required'
        ]);

        $data = [
            'kode' => $request->input('kode'),
            'nama' => $request->input('nama'),
            'status' => $request->input('status'),
            'keterangan' => $request->input('keterangan'),
            'user_id' => Auth::user()->id,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Pdam::insert($data);

        if ($service) 
        {
            return redirect('/pdam');
        }
        else 
        {
            return redirect('/pdam/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'nama' => 'required|min:0|max:150',
            'keterangan' => 'min:0|max:255',
            'status' => 'required'
        ]);

        $id = $request->input('id');

        $data = [
            'nama' => $request->input('nama'),
            'keterangan' => $request->input('keterangan'),
            'status' => $request->input('status'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Pdam::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/pdam');
        }
        else 
        {
            return redirect('/pdam/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = Pdam::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/pdam');
        }
        else 
        {
            return redirect('/pdam');
        }
    }
}
