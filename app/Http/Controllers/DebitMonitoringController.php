<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DebitMonitoring;

class DebitMonitoringController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = DebitMonitoring::GetAll(10);
        return view('debitmonitoring.index', ['title' => 'Data Monitoring Semua Debit', 'buttonClearAll' => false, 'data' => $data]);
    }

    public function monitoring($id)
    {
        $data = DebitMonitoring::GetById($id, 10);
        return view('debitmonitoring.index', ['title' => 'Data Monitoring Per Bulan', 'buttonClearAll' => true, 'debitId' => $id, 'data' => $data]);
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $data = DebitMonitoring::where(['id' => $id])->first();
        $service = DebitMonitoring::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/debit-monitoring/'. $data->debit_id);
        }
        else 
        {
            return redirect('/debit-monitoring/'. $id);
        }
    }

    public function deleteByDebitId(Request $request)
    {
        $this->validate($request, [
            'debit_id' => 'required',
        ]);

        $debit_id = $request->input('debit_id');

        $service = DebitMonitoring::where(['debit_id' => $debit_id])->delete();

        if ($service) 
        {
            return redirect('/debit-monitoring/' . $debit_id);
        }
        else 
        {
            return redirect('/debit-monitoring/' . $debit_id);
        }
    }
}
