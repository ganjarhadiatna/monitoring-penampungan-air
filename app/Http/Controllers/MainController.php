<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PdamMonitoring;
use App\Pdam;
use App\DebitMonitoring;
use App\Debit;
use App\Pembayaran;

class MainController extends Controller
{
    public function savePdam(Request $request)
    {
        $kode = $request->input('kode');
        $ph = $request->input('ph');
        $tds = $request->input('tds');
        $water_level = $request->input('water_level');
        $flow1 = $request->input('flow1');
        $flow2 = $request->input('flow2');

        $pdam = Pdam::where(['kode' => $kode])->first();

        $data = [
            'ph' => $ph,
            'tds' => $tds,
            'watter_lower' => $water_level,
            'camera' => 'http://example.com',
            'pdam_id' => $pdam->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];

        $service = PdamMonitoring::insert($data);

        if ($service) 
        {
            $debitPayload = [
                [
                    'ph' => '0',
                    'tds' => '0',
                    'watter_lower' => $flow1,
                    'debit_id' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'ph' => '0',
                    'tds' => '0',
                    'watter_lower' => $flow2,
                    'debit_id' => '2',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            ];

            $service = DebitMonitoring::insert($debitPayload);

            if ($service) 
            {
                $out = [
                    'status' => 'success',
                    'message' => 'success',
                    'pdam' => $data,
                    'debit' => $debitPayload,
                    'code' => 201
                ];
            }
            else 
            {
                $out = [
                    'status' => 'error',
                    'message' => 'error saving to debit',
                    'code' => 401
                ];
            }
        }
        else 
        {
            $out = [
                'status' => 'error',
                'message' => 'error saving to pdam',
                'code' => 401
            ];
        }

        return response()->json($out, $out['code']);
    }

    public function statusPembayaran(Request $request)
    {
        $id = $request->input('id');
        $data = Pembayaran::where(['id' => $id])->first();

        if ($data->status == '0')
        {
            Pembayaran::where(['id' => $id])->update(['status' => '1']);

            $out = [
                'message' => 'Sudah Dibayar',
                'status' => 'ok',
                'status_pembayaran' => '1',
                'code' => 201
            ];
        }
        else 
        {
            Pembayaran::where(['id' => $id])->update(['status' => '0']);

            $out = [
                'message' => 'Belum Dibayar',
                'status' => 'ok',
                'status_pembayaran' => '0',
                'code' => 201
            ];
        }

        return response()->json($out, $out['code']);
    }

    public function getPdam()
    {
        $data = Pdam::GetAllNoLimit();
        return response()->json($data);
    }

    public function getDebit()
    {
        $data = Debit::GetAllNoLimit();
        return response()->json($data);
    }

    public function infoDebit($id, $month)
    {
        $debit = Debit::where(['id' => $id])->first();
        $monitoring = DebitMonitoring::whereMonth('created_at', '=', $month)->where('debit_id', $id)->orderBy('id', 'desc')->first();
        $pembayaran = Pembayaran::where(['debit_id' => $id, 'bulan' => $month])->orderBy('id', 'desc')->first();
        $data = ['debit' => $debit, 'debitMonitoring' => $monitoring, 'pembayaran' => $pembayaran];
        return response()->json($data);
    }
}
