<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Debit;
use App\Pdam;
use Auth;

class DebitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Debit::GetAll(10);
        return view('debit.index', ['data' => $data]);
    }

    public function search()
    {
        $data = Debit::GetAllSearch(10, $_GET['search']);
        return view('debit.index', ['data' => $data]);
    }

    public function create()
    {
        $pdam = Pdam::get();
        return view('debit.form', ['pdam' => $pdam]);
    }

    public function edit($id)
    {
        $data = Debit::where(['id' => $id])->first();
        $pdam = Pdam::get();
        return view('debit.form', ['data' => $data, 'pdam' => $pdam]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'kode' => 'required|min:4|max:4',
            'nama' => 'required|min:0|max:150',
            'keterangan' => 'min:0|max:255',
            'pemilik' => 'required|min:0|max:150',
            'alamat' => 'min:0|max:255',
            'no_telp' => 'min:0|max:15',
            'pdam' => 'required',
            'status' => 'required'
        ]);

        $data = [
            'kode' => $request->input('kode'),
            'nama' => $request->input('nama'),
            'status' => $request->input('status'),
            'keterangan' => $request->input('keterangan'),
            'pemilik' => $request->input('pemilik'),
            'alamat' => $request->input('alamat'),
            'no_telp' => $request->input('no_telp'),
            'pdam_id' => $request->input('pdam'),
            'user_id' => Auth::user()->id,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Debit::insert($data);

        if ($service) 
        {
            return redirect('/debit');
        }
        else 
        {
            return redirect('/debit/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'nama' => 'required|min:0|max:150',
            'keterangan' => 'min:0|max:255',
            'pemilik' => 'required|min:0|max:150',
            'alamat' => 'min:0|max:255',
            'no_telp' => 'min:0|max:15',
            'pdam' => 'required',
            'status' => 'required'
        ]);

        $id = $request->input('id');

        $data = [
            'nama' => $request->input('nama'),
            'keterangan' => $request->input('keterangan'),
            'pemilik' => $request->input('pemilik'),
            'alamat' => $request->input('alamat'),
            'no_telp' => $request->input('no_telp'),
            'pdam_id' => $request->input('pdam'),
            'status' => $request->input('status'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Debit::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/debit');
        }
        else 
        {
            return redirect('/debit/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = Debit::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/debit');
        }
        else 
        {
            return redirect('/debit');
        }
    }
}
