<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use App\Charts\PdamChart;
use App\Charts\DebitChart;
use App\DebitMonitoring;
use App\PdamMonitoring;
use App\Pdam;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $fillColors = [
            "rgba(255, 99, 132, 0.2)",
            "rgba(22,160,133, 0.2)",
            "rgba(255, 205, 86, 0.2)",
            "rgba(51,105,232, 0.2)",
            "rgba(244,67,54, 0.2)",
            "rgba(34,198,246, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(233,30,99, 0.2)",
            "rgba(205,220,57, 0.2)"

        ];

        $tgl = Carbon::now()->day();
        $dataPdam = [];
        $tglPdam = [];
        $pdam = PdamMonitoring::whereDay('created_at', '<=', $tgl)->limit(20)->get();
        foreach ($pdam as $dt) {
            array_push($dataPdam, $dt->watter_lower);
            array_push($tglPdam, date_format($dt->created_at, "d"));
        }

        $dataDebit = [];
        $tglDebit = [];
        $debit = DebitMonitoring::where(['debit_id' => '1'])->whereDay('created_at', '<=', $tgl)->limit(10)->get();
        foreach ($debit as $dt) {
            array_push($dataDebit, $dt->watter_lower);
            array_push($tglDebit, date_format($dt->created_at, "d"));
        }

        $dataDebit2 = [];
        $tglDebit2 = [];
        $debit2 = DebitMonitoring::where(['debit_id' => '2'])->whereDay('created_at', '<=', $tgl)->limit(10)->get();
        foreach ($debit2 as $dt) {
            array_push($dataDebit2, $dt->watter_lower);
            array_push($tglDebit2, date_format($dt->created_at, "d"));
        }


        // $debit2 = DebitMonitoring::where(['debit_id' => '2'])->whereDay('created_at', '<=', $tgl)->limit(10)->get();

        $pdamChart = new PdamChart();
        $pdamChart->labels($tglPdam);
        $pdamChart->dataset('Monitoring Penampungan Air', 'line', $dataPdam)
        ->backgroundcolor($fillColors);

        $debitChart = new DebitChart();
        $debitChart->labels(['Debi 1', 'Debit 2']);
        $debitChart->dataset('Monitoring Per Debit Air', 'bar', [$dataDebit, $dataDebit2])
        ->backgroundcolor($fillColors);

        // return view('home', ['pdamChart' => $pdamChart, 'debitChart' => $debitChart]);
        $data = Pdam::GetAll(5);
        return view('home', ['data' => $data]);
    }
}
