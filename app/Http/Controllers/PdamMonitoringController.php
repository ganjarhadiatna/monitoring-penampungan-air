<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PdamMonitoring;

class PdamMonitoringController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = PdamMonitoring::GetAll(10);
        return view('pdammonitoring.index', ['title' => 'Monitoring Penampungan Air', 'buttonClearAll' => true, 'data' => $data]);
    }

    public function monitoring($id)
    {
        $data = PdamMonitoring::GetById($id, 10);
        return view('pdammonitoring.index', ['title' => 'Monitoring Penampungan Air Per Alat', 'buttonClearAll' => true, 'pdamId' => $id, 'data' => $data]);
    }

    public function deleteAll(Request $request)
    {
        $service = PdamMonitoring::truncate();

        if ($service) 
        {
            return redirect('/pdam-monitoring');
        }
        else 
        {
            return redirect('/pdam-monitoring');
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = PdamMonitoring::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/pdam-monitoring');
        }
        else 
        {
            return redirect('/pdam-monitoring');
        }
    }
}
