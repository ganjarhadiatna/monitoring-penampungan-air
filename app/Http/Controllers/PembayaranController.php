<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pembayaran;
use App\Debit;
use App\DebitMonitoring;
use Carbon\Carbon;
use Auth;

class PembayaranController extends Controller
{
    protected $month = [
        ['id' => '1', 'month' => 'Januari'],
        ['id' => '2', 'month' => 'Februari'],
        ['id' => '3', 'month' => 'Maret'],
        ['id' => '4', 'month' => 'April'],
        ['id' => '5', 'month' => 'Mei'],
        ['id' => '6', 'month' => 'Juni'],
        ['id' => '7', 'month' => 'Juli'],
        ['id' => '8', 'month' => 'Agustus'],
        ['id' => '9', 'month' => 'September'],
        ['id' => '10', 'month' => 'Oktober'],
        ['id' => '11', 'month' => 'November'],
        ['id' => '12', 'month' => 'Desember']
    ];

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Pembayaran::GetAll(10);
        return view('pembayaran.index', ['data' => $data, 'month' => $this->month, 'title' => 'Pembayaran']);
    }

    public function search()
    {
        $data = Pembayaran::GetAllSearch(10, $_GET['search']);
        return view('pembayaran.index', ['data' => $data, 'month' => $this->month, 'title' => 'Cari: ' . $_GET['search']]);
    }

    public function create()
    {
        $debit = Debit::get();
        return view('pembayaran.form', ['debit' => $debit, 'month' => $this->month]);
    }

    public function edit($id)
    {
        $data = Pembayaran::where(['id' => $id])->first();
        $debit = Debit::get();
        return view('pembayaran.form', ['data' => $data, 'debit' => $debit, 'month' => $this->month]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'bulan' => 'required',
            'jumlah' => 'required|min:0|max:32',
            'biaya' => 'required|min:0|max:32',
            'bayar' => 'required|min:0|max:32',
            'debit' => 'required',
            'status' => 'required'
        ]);

        $data = [
            'bulan' => $request->input('bulan'),
            'jumlah' => $request->input('jumlah'),
            'biaya' => $request->input('biaya'),
            'bayar' => $request->input('bayar'),
            'status' => $request->input('status'),
            'debit_id' => $request->input('debit'),
            'user_id' => Auth::user()->id,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $tgl = $request->input('bulan');
        $tgl2 = Date('m');

        if ($tgl <= $tgl2) {
            // DebitMonitoring::where(['debit_id' => $request->input('debit')])->whereMonth('created_at', $tgl)->delete();
            $service = Pembayaran::insert($data);

            if ($service) 
            {
                return redirect('/pembayaran');
            }
            else 
            {
                return redirect('/pembayaran/create');
            }
        } else {
            return redirect('/pembayaran/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'bulan' => 'required',
            'jumlah' => 'required|min:0|max:32',
            'biaya' => 'required|min:0|max:32',
            'bayar' => 'required|min:0|max:32',
            'debit' => 'required',
            'status' => 'required'
        ]);

        $id = $request->input('id');

        $data = [
            'bulan' => $request->input('bulan'),
            'jumlah' => $request->input('jumlah'),
            'biaya' => $request->input('biaya'),
            'bayar' => $request->input('bayar'),
            'status' => $request->input('status'),
            'debit_id' => $request->input('debit'),
            'user_id' => Auth::user()->id,
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Pembayaran::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/pembayaran');
        }
        else 
        {
            return redirect('/pembayaran/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = Pembayaran::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/pembayaran');
        }
        else 
        {
            return redirect('/pembayaran');
        }
    }
}
