<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PdamMonitoring extends Model
{
    protected $table = "pdam_monitoring";

    public function scopeGetAll($query, $limit)
    {
        return $this
        ->select(
            'pdam_monitoring.id',
            'pdam_monitoring.ph',
            'pdam_monitoring.tds',
            'pdam_monitoring.watter_lower',
            'pdam_monitoring.camera',
            'pdam_monitoring.created_at',
            'pdam_monitoring.updated_at',
            'pdam.nama as nama_alat',
            'users.name as nama_admin'
        )
        ->join('pdam', 'pdam.id', '=', 'pdam_monitoring.pdam_id')
        ->join('users', 'users.id', '=', 'pdam.user_id')
        ->orderBy('id', 'desc')
        ->paginate(10);
    }

    public function scopeGetById($query, $id, $limit)
    {
        return $this
        ->select(
            'pdam_monitoring.id',
            'pdam_monitoring.ph',
            'pdam_monitoring.tds',
            'pdam_monitoring.watter_lower',
            'pdam_monitoring.camera',
            'pdam_monitoring.created_at',
            'pdam_monitoring.updated_at',
            'pdam.nama as nama_alat',
            'users.name as nama_admin'
        )
        ->where('pdam_monitoring.pdam_id', $id)
        ->join('pdam', 'pdam.id', '=', 'pdam_monitoring.pdam_id')
        ->join('users', 'users.id', '=', 'pdam.user_id')
        ->orderBy('id', 'desc')
        ->paginate(10);
    }
}
