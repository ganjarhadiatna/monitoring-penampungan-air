<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pdam extends Model
{
    protected $table = "pdam";

    public function scopeGetAll($query, $limit)
    {
        return $this
        ->select(
            'pdam.id',
            'pdam.kode',
            'pdam.nama',
            'pdam.keterangan',
            'pdam.status',
            'pdam.created_at',
            'pdam.updated_at',
            'users.name as nama_admin',
            (DB::raw('(select ph from pdam_monitoring where pdam_id=pdam.id order by id desc limit 1) as ph ')),
            (DB::raw('(select tds from pdam_monitoring where pdam_id=pdam.id order by id desc limit 1) as tds ')),
            (DB::raw('(select watter_lower from pdam_monitoring where pdam_id=pdam.id order by id desc limit 1) as watter_lower '))
        )
        ->join('users', 'users.id', '=', 'pdam.user_id')
        ->orderBy('id', 'desc')
        ->paginate(10);
    }

    public function scopeGetAllNoLimit($query)
    {
        return $this
        ->select(
            'pdam.id',
            'pdam.kode',
            'pdam.nama',
            'pdam.keterangan',
            'pdam.status',
            'pdam.created_at',
            'pdam.updated_at',
            'users.name as nama_admin',
            (DB::raw('(select ph from pdam_monitoring where pdam_id=pdam.id order by id desc limit 1) as ph ')),
            (DB::raw('(select tds from pdam_monitoring where pdam_id=pdam.id order by id desc limit 1) as tds ')),
            (DB::raw('(select watter_lower from pdam_monitoring where pdam_id=pdam.id order by id desc limit 1) as watter_lower '))
        )
        ->join('users', 'users.id', '=', 'pdam.user_id')
        ->orderBy('id', 'desc')
        ->get();
    }
}
