<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DebitMonitoring extends Model
{
    protected $table = "debit_monitoring";

    public function scopeGetAll($query, $limit)
    {
        return $this
        ->select(
            'debit_monitoring.id',
            'debit_monitoring.ph',
            'debit_monitoring.tds',
            'debit_monitoring.watter_lower',
            'debit_monitoring.created_at',
            'debit_monitoring.updated_at',
            'debit.nama'
        )
        ->join('debit', 'debit.id', '=', 'debit_monitoring.debit_id')
        ->orderBy('id', 'desc')
        ->paginate(10);
    }

    public function scopeGetById($query, $id, $limit)
    {
        return $this
        ->select(
            'debit_monitoring.id',
            'debit_monitoring.ph',
            'debit_monitoring.tds',
            'debit_monitoring.watter_lower',
            'debit_monitoring.created_at',
            'debit_monitoring.updated_at',
            'debit.nama'
        )
        ->where('debit_monitoring.debit_id', $id)
        ->join('debit', 'debit.id', '=', 'debit_monitoring.debit_id')
        ->orderBy('id', 'desc')
        ->paginate(10);
    }
}
