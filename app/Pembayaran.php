<?php   
namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $table = "pembayaran";

    // protected $fillable = [];

    // public $timestamps = false;

    public function scopeGetAll($query, $limit)
    {
        return $this
        ->select(
            'pembayaran.id',
            'pembayaran.jumlah',
            'pembayaran.biaya',
            'pembayaran.bayar',
            'pembayaran.status',
            'pembayaran.bulan',
            'pembayaran.created_at',
            'pembayaran.updated_at',
            'users.name as nama_admin',
            'debit.nama as nama_debit',
            'debit.pemilik as pemilik'
        )
        ->join('users', 'users.id', '=', 'pembayaran.user_id')
        ->join('debit', 'debit.id', '=', 'pembayaran.debit_id')
        ->orderBy('id', 'desc')
        ->paginate(10);
    }

    public function scopeGetAllSearch($query, $limit, $search)
    {
        return $this
        ->select(
            'pembayaran.id',
            'pembayaran.jumlah',
            'pembayaran.biaya',
            'pembayaran.bayar',
            'pembayaran.status',
            'pembayaran.bulan',
            'pembayaran.created_at',
            'pembayaran.updated_at',
            'users.name as nama_admin',
            'debit.nama as nama_debit',
            'debit.pemilik as pemilik'
        )
        ->join('users', 'users.id', '=', 'pembayaran.user_id')
        ->join('debit', 'debit.id', '=', 'pembayaran.debit_id')
        ->where('debit.pemilik', 'like', '%' . $search . '%')
        ->orWhere('debit.nama', 'like', '%' . $search . '%')
        ->orderBy('id', 'desc')
        ->paginate(10);
    }
}