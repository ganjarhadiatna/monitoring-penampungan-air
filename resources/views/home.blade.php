@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        <h3 style="margin-top: 6px;">Dashboard</h3>
                    </div>
                    <div class="ml-auto">
                        <a href="https://www.dropbox.com/sh/qdagtl6cmdus9mi/AABZBb2p8ON1u8ayFctgMU8Ka?dl=0" target="_blank" class="btn btn-primary">
                            Penyimpanan Kamera
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    @foreach($data as $dt)
                        <div class="wrapper">
                            <div style="width: 100%;">
                                <div class="card">
                                    <div class="card-body wrapper">
                                        <div class="mr-auto">
                                            <i class="fa fa-4x fa-chart-line" style="color: red;"></i>
                                        </div>
                                        <div class="ml-auto" style="text-align: right;">
                                            <h4>{{ $dt->ph ? $dt->ph : '0' }}</h4>
                                            <p>PH</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="width: 100%; margin: 0 15px;">
                                <div class="card">
                                    <div class="card-body wrapper">
                                        <div class="mr-auto">
                                            <i class="fa fa-4x fa-chart-line" style="color: red;"></i>
                                        </div>
                                        <div class="ml-auto" style="text-align: right;">
                                            <h4>{{ $dt->tds ? $dt->tds : '0' }} PPM</h4>
                                            <p>TDS</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="width: 100%;">
                                <div class="card">
                                    <div class="card-body wrapper">
                                        <div class="mr-auto">
                                            <i class="fa fa-4x fa-chart-line" style="color: red;"></i>
                                        </div>
                                        <div class="ml-auto" style="text-align: right;">
                                            <h4>{{ $dt->watter_lower ? $dt->watter_lower : '0' }} %</h4>
                                            <p>WATER LEVEL</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 20px;">
                            @if($dt->tds < 300)
                                <div class="alert alert-primary" role="alert" id="alert-success">
                                    Air Baik Untuk Diminum
                                </div>
                            @endif

                            @if($dt->tds >= 300 && $dt->tds < 600)
                                <div class="alert alert-primary" role="alert" id="alert-danger">
                                    Air Bisa Diminum
                                </div>
                            @endif

                            @if($dt->tds >= 600 && $dt->tds < 900)
                                <div class="alert alert-warning" role="alert" id="alert-danger">
                                    Air Dalam Keadaan Rata - rata
                                </div>
                            @endif

                            @if($dt->tds >= 900)
                                <div class="alert alert-danger" role="alert" id="alert-danger">
                                    Air Berbahaya
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
