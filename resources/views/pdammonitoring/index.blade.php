@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        <h3 style="margin-top: 6px;">
                            {{ $title }}
                        </h3>
                    </div>
                    <div class="ml-auto">
                        @if($buttonClearAll)
                            <button class="btn btn-danger" data-toggle="modal" data-target="#deleteAllModal">
                                <i class="fa fa-lw fa-trash-alt"></i>
                            </button>

                            <div class="modal fade" id="deleteAllModal" tabindex="-1" role="dialog" aria-labelledby="deleteAllModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="deleteAllModalLabel">Peringatan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Semua data monitoring dihapus secara permanen, lanjutkan?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>

                                        <a class="btn btn-danger" 
                                            href="{{ isset($pdamId) ? route('ui-pdam-monitoring-delete') : route('ui-pdam-monitoring-delete-all') }}"
                                            onclick="event.preventDefault(); document.getElementById('id-form-{{ isset($pdamId) ? $pdamId : 0 }}').submit();"
                                            >
                                            Lanjutkan
                                        </a>

                                        <form id="id-form-{{ isset($pdamId) ? $pdamId : 0 }}" action="{{ isset($pdamId) ? route('ui-pdam-monitoring-delete') : route('ui-pdam-monitoring-delete-all') }}" method="POST" style="display: block;">
                                            @csrf
                                            <input type="hidden" name="debit_id" value="{{ isset($pdamId) ? $pdamId : 0 }}" />
                                        </form>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">PH</th>
                                <th scope="col">TDS</th>
                                <th scope="col">Water Level</th>
                                <!-- <th scope="col">Penampungan Air</th>
                                <th scope="col">Admin</th> -->
                                <th scope="col">Tanggal Dibuat</th>
                                <th scope="col">Tanggal Diubah</th>
                                <th width="70"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $dt)
                                <tr>
                                    <th scope="row">{{ $i }}</th>
                                    <td>{{ $dt->ph }}</td>
                                    <td>{{ $dt->tds }} ppm</td>
                                    <td>{{ $dt->watter_lower }} %</td>
                                    <!-- <td>{{ $dt->nama_alat }}</td>
                                    <td>{{ $dt->nama_admin }}</td> -->
                                    <td>{{ $dt->created_at }}</td>
                                    <td>{{ $dt->updated_at }}</td>
                                    <td>
                                        <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $dt->id }}">
                                            <i class="fa fa-lw fa-trash-alt"></i>
                                        </button>

                                        <div class="modal fade" id="deleteModal{{ $dt->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel{{ $dt->id }}" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="deleteModalLabel{{ $dt->id }}">Peringatan</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Data akan dihapus secara permanen, lanjutkan?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>

                                                    <a class="btn btn-danger" 
                                                        href="{{ route('ui-pdam-monitoring-delete') }}"
                                                        onclick="event.preventDefault(); document.getElementById('id-form-{{ $dt->id }}').submit();"
                                                        >
                                                        Lanjutkan
                                                    </a>

                                                    <form id="id-form-{{ $dt->id }}" action="{{ route('ui-pdam-monitoring-delete') }}" method="POST" style="display: block;">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{ $dt->id }}" />
                                                    </form>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Peringatan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Divisi akan dihapus secara permanen, lanjutkan?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-primary">Lanjutkan</button>
      </div>
    </div>
  </div>
</div>

<script>
function () {

}
</script>

@endsection
