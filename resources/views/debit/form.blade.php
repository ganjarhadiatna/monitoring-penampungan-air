@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Debit</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Debit</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-debit-update') }} @else {{ route('ui-debit-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif
                        
                        <div class="form-group">
                            <label for="inputKode">KODE</label>
                            @if(isset($data))
                                <input type="text" value="@if(isset($data)) {{ $data->kode }} @endif" class="form-control @error('kode') is-invalid @enderror" id="inputKode" name="kode" required readonly>
                            @else
                                <input type="text" value="@if(isset($data)) {{ $data->kode }} @endif" class="form-control @error('kode') is-invalid @enderror" id="inputKode" name="kode" required>
                            @endif
                            @error('kode')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        
                        <div class="form-group">
                            <label for="inputNama">Nama debit</label>
                            <input type="text" value="@if(isset($data)) {{ $data->nama }} @endif" class="form-control @error('nama') is-invalid @enderror" id="inputNama" name="nama" required>
                            @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputKeterangan">Keterangan debit</label>
                            <input type="text" value="@if(isset($data)) {{ $data->keterangan }} @endif" class="form-control @error('keterangan') is-invalid @enderror" id="inputKeterangan" name="keterangan">
                            @error('keterangan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputPemilik">Nama pemilik</label>
                            <input type="text" value="@if(isset($data)) {{ $data->pemilik }} @endif" class="form-control @error('pemilik') is-invalid @enderror" id="inputPemilik" name="pemilik" required>
                            @error('pemilik')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputAlamat">Alamat</label>
                            <input type="text" value="@if(isset($data)) {{ $data->alamat }} @endif" class="form-control @error('alamat') is-invalid @enderror" id="inputAlamat" name="alamat">
                            @error('alamat')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputNoTelp">No. Telpon</label>
                            <input type="text" value="@if(isset($data)) {{ $data->no_telp }} @endif" class="form-control @error('no_telp') is-invalid @enderror" id="inputNoTelp" name="no_telp">
                            @error('no_telp')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputPdam">Penampungan Air</label>
                                <select value="@if(isset($data)) {{ $data->pdam }} @endif" class="form-control @error('pdam') is-invalid @enderror" id="inputPdam" name="pdam">
                                    @foreach($pdam as $pd)
                                        <option value="{{ $pd->id }}">{{ $pd->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('pdam')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputStatus">Status alat</label>
                                <select value="@if(isset($data)) {{ $data->status }} @endif" class="form-control @error('status') is-invalid @enderror" id="inputStatus" name="status">
                                @if(isset($data))
                                    @if($data->status == 0)
                                        <option value="0" selected>Mati</option>
                                        <option value="1">Hidup</option>
                                    @else
                                        <option value="0">Mati</option>
                                        <option value="1" selected>Hidup</option>
                                    @endif
                                @else
                                        <option value="0">Mati</option>
                                        <option value="1">Hidup</option>
                                @endif
                                </select>
                            </div>
                            @error('status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
