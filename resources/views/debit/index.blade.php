@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        <h3 style="margin-top: 6px;">Daftar Debit Air</h3>
                    </div>
                    <div class="ml-auto">
                        <a href="{{ route('ui-debit-create') }}" class="btn btn-primary">
                            <i class="fa fa-lw fa-plus"></i>
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Kode</th>
                                <th scope="col">Debit</th>
                                <th scope="col">Pemilik</th>
                                <th scope="col">Penggunaan Air (L)</th>
                                <th scope="col">Status Alat</th>
                                <th scope="col">Penampungan Air</th>
                                <th scope="col">Dibuat Oleh</th>
                                <th scope="col">Tanggal Dibuat</th>
                                <th width="160"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $dt)
                                <tr>
                                    <th scope="row">{{ $i }}</th>
                                    <td>{{ $dt->kode }}</td>
                                    <td>{{ $dt->nama }}</td>
                                    <td>{{ $dt->pemilik }}</td>
                                    <td id="data-wl-{{ $dt->id }}">{{ $dt->watter_lower ? ($dt->watter_lower) : '0' }} Liter</td>
                                    <td>{{ $dt->status == 0 ?'Mati' : 'Hidup' }}</td>
                                    <td>{{ $dt->nama_alat }}</td>
                                    <td>{{ $dt->nama_admin }}</td>
                                    <td>{{ $dt->created_at }}</td>
                                    <td>
                                        <a href="{{ route('ui-debit-monitoring-byid', $dt->id) }}" class="btn btn-primary">
                                            <i class="fa fa-lw fa-chart-line"></i>
                                        </a>
                                        
                                        <a href="{{ route('ui-debit-edit', $dt->id) }}" class="btn btn-warning">
                                            <i class="fa fa-lw fa-pencil-alt"></i>
                                        </a>

                                        <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $dt->id }}">
                                            <i class="fa fa-lw fa-trash-alt"></i>
                                        </button>

                                        <div class="modal fade" id="deleteModal{{ $dt->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel{{ $dt->id }}" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="deleteModalLabel{{ $dt->id }}">Peringatan</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Data akan dihapus secara permanen, lanjutkan?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>

                                                    <a class="btn btn-danger" 
                                                        href="{{ route('ui-debit-delete') }}"
                                                        onclick="event.preventDefault(); document.getElementById('id-form-{{ $dt->id }}').submit();"
                                                        >
                                                        Lanjutkan
                                                    </a>

                                                    <form id="id-form-{{ $dt->id }}" action="{{ route('ui-debit-delete') }}" method="POST" style="display: block;">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{ $dt->id }}" />
                                                    </form>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
            
        </div>
    </div>
</div>

<script>
    function getMonitoring () {
        $.ajax({
            url: "{{ url('api/debit/get') }}",
            dataType: 'json'
        }).done(function(data) {
            var dt = data;
            for (let index = 0; index < dt.length; index++) {
                const element = dt[index];
                $('#data-wl-' + element.id).text(element.watter_lower > 0 ? (element.watter_lower) + ' Liter' : '0 Liter');
            }
        });
    }

    $(document).ready(function () {
        setInterval(() => {
            getMonitoring();
        }, 2000);
    });
</script>

@endsection
