@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        <h3 style="margin-top: 6px;">{{ $title }}</h3>
                    </div>
                    <div class="ml-auto">
                        <form mtehod="GET" action="{{ route('ui-pembayaran-search') }}" style="width: 500px; display: inline-block; vertical-align: top;">
                            <input id="search" type="search" class="form-control @error('search') is-invalid @enderror" name="search" value="{{ isset($_GET['search']) ? $_GET['search'] : old('searc') }}" required placeholder="Cari berdasarkan debit atau pemilik.." autocomplete="search">
                        </form>
                        <a href="{{ route('ui-pembayaran-create') }}" class="btn btn-primary">
                            <i class="fa fa-lw fa-plus"></i>
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Debit</th>
                                <th scope="col">Pemilik</th>
                                <th scope="col">Penggunaan Air (L)</th>
                                <th scope="col">Biaya/Kubik (m<sup>3</sup>)</th>
                                <th scope="col">Total Tagihan</th>
                                <th scope="col">Bulan</th>
                                <th scope="col">Dibuat Oleh</th>
                                <th scope="col">Tanggal Dibuat</th>
                                <th scope="col">Status</th>
                                <th width="120"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $dt)
                                <tr>
                                    <th scope="row">{{ $i }}</th>
                                    <td>{{ $dt->nama_debit }}</td>
                                    <td>{{ $dt->pemilik }}</td>
                                    <td>{{ $dt->jumlah }} Liter</td>
                                    <td>Rp. {{ $dt->biaya }}</td>
                                    <td>Rp. {{ $dt->bayar }}</td>
                                    <td>
                                        @foreach($month as $mt)
                                            @if($mt['id'] == $dt->bulan)
                                                {{ $mt['month'] }}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>{{ $dt->nama_admin }}</td>
                                    <td>{{ $dt->created_at }}</td>
                                    <td>
                                        @if($dt->status == 0) 
                                            <button id="button-pembayaran-{{ $dt->id }}" class="btn btn-danger" onclick="setPembayaran({{ $dt->id }})">
                                                Belum Bayar
                                            </button>
                                        @endif 
                                        @if($dt->status == 1) 
                                            <button id="button-pembayaran-{{ $dt->id }}" class="btn btn-success" onclick="setPembayaran({{ $dt->id }})">
                                                Sudah Bayar
                                            </button>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('ui-pembayaran-edit', $dt->id) }}" class="btn btn-warning">
                                            <i class="fa fa-lw fa-pencil-alt"></i>
                                        </a>
                                        
                                        <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $dt->id }}">
                                            <i class="fa fa-lw fa-trash-alt"></i>
                                        </button>

                                        <div class="modal fade" id="deleteModal{{ $dt->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel{{ $dt->id }}" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="deleteModalLabel{{ $dt->id }}">Peringatan</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Data akan dihapus secara permanen, lanjutkan?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>

                                                    <a class="btn btn-danger" 
                                                        href="{{ route('ui-pembayaran-delete') }}"
                                                        onclick="event.preventDefault(); document.getElementById('id-form-{{ $dt->id }}').submit();"
                                                        >
                                                        Lanjutkan
                                                    </a>

                                                    <form id="id-form-{{ $dt->id }}" action="{{ route('ui-pembayaran-delete') }}" method="POST" style="display: block;">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{ $dt->id }}" />
                                                    </form>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="bayarModel" tabindex="-1" role="dialog" aria-labelledby="bayarModelLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="bayarModelLabel">Peringatan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Masih on Progress
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-primary">Lanjutkan</button>
      </div>
    </div>
  </div>
</div>

<script>
    function setPembayaran (id) {
        $.ajax({
            url: "{{ url('api/pembayaran/status') }}",
            dataType: 'json',
            type: 'POST',
            data: {
                'id': id
            }
        }).done(function(data) {
            console.log(data);
            var dt = data.status_pembayaran;
            if (dt == '1') {
                $('#button-pembayaran-' + id).removeClass('btn btn-danger').addClass('btn btn-success').text('Sudah Bayar');
            } else {
                $('#button-pembayaran-' + id).removeClass('btn btn-success').addClass('btn btn-danger').text('Belum Bayar');
            }
        });
    }
</script>

@endsection
