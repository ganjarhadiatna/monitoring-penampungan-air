@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Pembayaran</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Pembayaran</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-pembayaran-update') }} @else {{ route('ui-pembayaran-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif

                        <div class="alert alert-danger" role="alert" id="alert-danger" style="display: none;">
                            This is a danger alert—check it out!
                        </div>

                        <div class="alert alert-primary" role="alert" id="alert-success" style="display: none;">
                            This is a primary alert—check it out!
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputBulan">Pilih Bulan</label>
                                <select value="@if(isset($data)) {{ $data->bulan }} @endif" class="form-control @error('bulan') is-invalid @enderror" id="inputBulan" name="bulan">
                                    @foreach($month as $dt)
                                        @if($dt['id'] <= Date('m'))
                                            <option value="{{ $dt['id'] }}">{{ $dt['month'] }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            @error('bulan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputDebit">Pemilik debit air</label>
                                <select value="@if(isset($data)) {{ $data->debit }} @endif" class="form-control @error('debit') is-invalid @enderror" id="inputDebit" name="debit">
                                    <option value="0">Pilih pemilik debit air</option>
                                    @foreach($debit as $pd)
                                        <option value="{{ $pd->id }}">{{ $pd->nama . ' : ' . $pd->pemilik }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('debit')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        
                        <div class="form-group">
                            <label for="inputJumlah">Jumlah (liter)</label>
                            <input type="text" value="@if(isset($data)) {{ $data->jumlah }} @endif" class="form-control @error('jumlah') is-invalid @enderror" id="inputJumlah" name="jumlah" required>
                            @error('jumlah')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        
                        <div class="form-group">
                            <label for="inputBiaya">Biaya (Rp)</label>
                            <input type="text" value="@if(isset($data)) {{ $data->biaya }} @endif" class="form-control @error('biaya') is-invalid @enderror" id="inputBiaya" name="biaya" required>
                            @error('biaya')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputBayar">Bayar (Rp)</label>
                            <input type="text" value="@if(isset($data)) {{ $data->bayar }} @endif" class="form-control @error('bayar') is-invalid @enderror" id="inputBayar" name="bayar" required>
                            @error('bayar')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputStatus">Status pembayaran</label>
                                <select value="@if(isset($data)) {{ $data->status }} @endif" class="form-control @error('status') is-invalid @enderror" id="inputStatus" name="status">
                                @if(isset($data))
                                    @if($data->status == 0)
                                        <option value="0" selected>Belum Bayar</option>
                                        <option value="1">Sudah Bayar</option>
                                    @else
                                        <option value="0">Belum Bayar</option>
                                        <option value="1" selected>Sudah Bayar</option>
                                    @endif
                                @else
                                        <option value="0">Belum Bayar</option>
                                        <option value="1">Sudah Bayar</option>
                                @endif
                                </select>
                            </div>
                            @error('status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>

<script>
    function cekPembayaran () {
        var bulan = $('#inputBulan').val();
        var pemilik = $('#inputDebit').val();
        $('#alert-danger').hide();
        $('#alert-success').hide();
        $.ajax({
            url: "{{ url('api/debit/info') }}" + '/' + pemilik + '/' + bulan,
            dataType: "JSON"
        }).then(function (data) {
            console.log(data);
            if (data.pembayaran) {
                var jumlah = data.debitMonitoring.watter_lower;
                var kubik = (data.debitMonitoring.watter_lower);
                var biaya = jumlah <= 10 ? 3500 : jumlah <= 20 ? 4000 : 5000;
                var bayar = (biaya * kubik);

                $('#inputJumlah').attr('value', kubik);
                $('#inputBiaya').attr('value', biaya);
                $('#inputBayar').attr('value', bayar);
                $('#alert-success').show().html((data.pembayaran.status == '1') ? 'Pemilik sudah membayar tagihan.' : 'Pemilik belum membayar tagihan.');

                if (data.pembayaran.status == '1') {
                    $('#inputStatus option[value="1"]').attr('selected', true);
                    $('#inputStatus option[value="0"]').attr('selected', false);
                } else {
                    $('#inputStatus option[value="1"]').attr('selected', false);
                    $('#inputStatus option[value="0"]').attr('selected', true);
                }
            } else {
                if (data.debitMonitoring) {
                    var jumlah = data.debitMonitoring.watter_lower;
                    var kubik = (data.debitMonitoring.watter_lower);
                    var biaya = jumlah <= 10 ? 3500 : jumlah <= 20 ? 4000 : 5000;
                    var bayar = (biaya * kubik);

                    $('#inputJumlah').attr('value', kubik);
                    $('#inputBiaya').attr('value', biaya);
                    $('#inputBayar').attr('value', bayar);
                    $('#inputStatus option[value="1"]').attr('selected', false);
                    $('#inputStatus option[value="0"]').attr('selected', true);
                } else {
                    $('#inputJumlah').attr('value', '');
                    $('#inputBiaya').attr('value', '');
                    $('#inputBayar').attr('value', '');
                    $('#inputStatus option[value="1"]').attr('selected', false);
                    $('#inputStatus option[value="0"]').attr('selected', true);

                    $('#alert-danger').show().html('Data monitoring tidak ditemukan.');
                }
            }

        }).catch(function (e) {
            console.log(e);
        })
    }
    $(document).ready(function () {
        $('#inputBulan').change(function (e) {
            cekPembayaran();
        });
        $('#inputDebit').change(function (e) {
            cekPembayaran();
        });
    });
</script>

@endsection
