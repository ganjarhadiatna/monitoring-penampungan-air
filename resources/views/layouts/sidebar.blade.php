<nav id="sidebar">
    <div class="sidebar-header">
        <h3>{{ 'Monitoring Penampungan Air' }}</h3>
    </div>

    <ul class="list-unstyled components">
        <li>
            <a href="{{ route('home') }}">Dashboard</a>
        </li>
        <!-- <li>
            <a href="{{ route('ui-pdam') }}">Penampungan Air</a>
        </li> -->
        <li>
            <a href="{{ route('ui-pdam-monitoring') }}">Penampungan Air</a>
        </li>
        <li>
            <a href="{{ route('ui-debit') }}">Debit Air</a>
        </li>
        <li>
            <!-- <a href="{{ route('ui-debit-monitoring') }}">Data Monitoring Debit</a> -->
        </li>
        <li>
            <a href="{{ route('ui-pembayaran') }}">Pembayaran</a>
        </li>
    </ul>
</nav>