@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        <h3 style="margin-top: 6px;">Dashboard</h3>
                    </div>
                    <div class="ml-auto">
                        <a href="https://www.dropbox.com/sh/qdagtl6cmdus9mi/AABZBb2p8ON1u8ayFctgMU8Ka?dl=0" target="_blank" class="btn btn-primary">
                            Penyimpanan Kamera
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <div style="width: 100%; margin-bottom: 20px;">
                        {!! $pdamChart->container() !!}
                    </div>
                    <div style="width: 100%;">
                        {!! $debitChart->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- ChartScript --}}
@if($pdamChart)
    {!! $pdamChart->script() !!}
@endif

@if($debitChart)
    {!! $debitChart->script() !!}
@endif

@endsection
