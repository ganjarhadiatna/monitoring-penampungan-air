<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'pdam'], function () {
    // ui
    Route::get('', 'PdamController@index')->name('ui-pdam');
    Route::get('/create', 'PdamController@create')->name('ui-pdam-create');
    Route::get('/edit/{id}', 'PdamController@edit')->name('ui-pdam-edit');

    // crud
    Route::post('/save', 'PdamController@save')->name('ui-pdam-save');
    Route::post('/update', 'PdamController@update')->name('ui-pdam-update');
    Route::post('/delete', 'PdamController@delete')->name('ui-pdam-delete');
});

Route::group(['prefix' => 'pdam-monitoring'], function () {
    // ui
    Route::get('', 'PdamMonitoringController@index')->name('ui-pdam-monitoring');
    Route::get('/create', 'PdamMonitoringController@create')->name('ui-pdam-monitoring-create');
    Route::get('/edit/{id}', 'PdamMonitoringController@edit')->name('ui-pdam-monitoring-edit');
    Route::get('/{id}', 'PdamMonitoringController@monitoring')->name('ui-pdam-monitoring-byid');

    // crud
    Route::post('/save', 'PdamMonitoringController@save')->name('ui-pdam-monitoring-save');
    Route::post('/update', 'PdamMonitoringController@update')->name('ui-pdam-monitoring-update');
    Route::post('/delete', 'PdamMonitoringController@delete')->name('ui-pdam-monitoring-delete');
    Route::post('/delete-all', 'PdamMonitoringController@deleteAll')->name('ui-pdam-monitoring-delete-all');
    Route::post('/delete-by-pdam-id', 'DebitMonitoringController@deleteByPdamId')->name('ui-pdam-monitoring-delete-bypdamid');
});

Route::group(['prefix' => 'debit'], function () {
    // ui
    Route::get('', 'DebitController@index')->name('ui-debit');
    Route::get('/create', 'DebitController@create')->name('ui-debit-create');
    Route::get('/edit/{id}', 'DebitController@edit')->name('ui-debit-edit');

    // crud
    Route::post('/save', 'DebitController@save')->name('ui-debit-save');
    Route::post('/update', 'DebitController@update')->name('ui-debit-update');
    Route::post('/delete', 'DebitController@delete')->name('ui-debit-delete');
});

Route::group(['prefix' => 'debit-monitoring'], function () {
    // ui
    Route::get('', 'DebitMonitoringController@index')->name('ui-debit-monitoring');
    Route::get('/create', 'DebitMonitoringController@create')->name('ui-debit-monitoring-create');
    Route::get('/edit/{id}', 'DebitMonitoringController@edit')->name('ui-debit-monitoring-edit');
    Route::get('/{id}', 'DebitMonitoringController@monitoring')->name('ui-debit-monitoring-byid');

    // crud
    Route::post('/save', 'DebitMonitoringController@save')->name('ui-debit-monitoring-save');
    Route::post('/update', 'DebitMonitoringController@update')->name('ui-debit-monitoring-update');
    Route::post('/delete', 'DebitMonitoringController@delete')->name('ui-debit-monitoring-delete');
    Route::post('/delete-by-debit-id', 'DebitMonitoringController@deleteByDebitId')->name('ui-debit-monitoring-delete-bydebitid');
});

Route::group(['prefix' => 'pembayaran'], function () {
    // ui
    Route::get('', 'PembayaranController@index')->name('ui-pembayaran');
    Route::get('/search', 'PembayaranController@search')->name('ui-pembayaran-search');
    Route::get('/create', 'PembayaranController@create')->name('ui-pembayaran-create');
    Route::get('/edit/{id}', 'PembayaranController@edit')->name('ui-pembayaran-edit');

    // crud
    Route::post('/save', 'PembayaranController@save')->name('ui-pembayaran-save');
    Route::post('/update', 'PembayaranController@update')->name('ui-pembayaran-update');
    Route::post('/delete', 'PembayaranController@delete')->name('ui-pembayaran-delete');
});
