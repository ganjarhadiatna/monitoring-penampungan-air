<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePdamMonitoringTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pdam_monitoring', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ph');
            $table->string('tds');
            $table->string('watter_lower');
            $table->string('camera')->nullable();
            $table->unsignedBigInteger('pdam_id');
            $table->timestamps();

            $table->foreign('pdam_id')->references('id')->on('pdam');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pdam_monitoring');
    }
}
