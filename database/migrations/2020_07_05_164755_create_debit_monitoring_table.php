<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDebitMonitoringTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debit_monitoring', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ph');
            $table->string('tds');
            $table->string('watter_lower');
            $table->unsignedBigInteger('debit_id');
            $table->timestamps();

            $table->foreign('debit_id')->references('id')->on('debit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debit_monitoring');
    }
}
