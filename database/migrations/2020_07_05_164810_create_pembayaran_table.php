<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembayaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembayaran', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('jumlah', 32);
            $table->string('biaya', 32);
            $table->string('bayar', 32);
            $table->string('bulan', 32);
            $table->enum('status', ['0', '1'])->default('0');
            $table->unsignedBigInteger('debit_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('debit_id')->references('id')->on('debit');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembayaran');
    }
}
