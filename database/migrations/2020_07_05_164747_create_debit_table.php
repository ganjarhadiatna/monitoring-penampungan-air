<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDebitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debit', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode', 4)->unique();
            $table->string('nama', 150);
            $table->string('keterangan', 255)->nullable();
            $table->string('pemilik', 150);
            $table->string('alamat', 255)->nullable();
            $table->string('no_telp', 15)->nullable();
            $table->enum('status', ['0', '1'])->default('0');
            $table->unsignedBigInteger('pdam_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('pdam_id')->references('id')->on('pdam');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debit');
    }
}
