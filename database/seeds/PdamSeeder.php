<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PdamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'kode' => 'P001',
                'nama' => 'PDAM 1',
                'keterangan' => 'PDAM 1',
                'status' => '1',
                'user_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'kode' => 'P002',
                'nama' => 'PDAM 2',
                'keterangan' => 'PDAM 2',
                'status' => '1',
                'user_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('pdam')->insert($data);
    }
}
