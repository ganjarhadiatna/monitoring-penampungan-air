<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DebitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'kode' => 'D001',
                'nama' => 'DEBIT AIR 1',
                'keterangan' => 'DEBIT AIR 1',
                'pemilik' => 'Mamat',
                'alamat' => 'Sukajadi 07/02',
                'no_telp' => '082000222111',
                'status' => '1',
                'pdam_id' => '1',
                'user_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'kode' => 'D002',
                'nama' => 'DEBIT AIR 2',
                'keterangan' => 'DEBIT AIR 2',
                'pemilik' => 'Suryana',
                'alamat' => 'Sukajadi 07/02',
                'no_telp' => '082000222111',
                'status' => '1',
                'pdam_id' => '1',
                'user_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('debit')->insert($data);
    }
}
