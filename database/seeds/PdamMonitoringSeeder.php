<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PdamMonitoringSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'ph' => '4102',
                'tds' => '2',
                'watter_lower' => '3',
                'camera' => 'http://example.com',
                'pdam_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'ph' => '4102',
                'tds' => '2',
                'watter_lower' => '3',
                'camera' => 'http://example.com',
                'pdam_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'ph' => '4102',
                'tds' => '2',
                'watter_lower' => '3',
                'camera' => 'http://example.com',
                'pdam_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => '4',
                'ph' => '4102',
                'tds' => '2',
                'watter_lower' => '3',
                'camera' => 'http://example.com',
                'pdam_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => '5',
                'ph' => '4102',
                'tds' => '2',
                'watter_lower' => '3',
                'camera' => 'http://example.com',
                'pdam_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => '6',
                'ph' => '4102',
                'tds' => '2',
                'watter_lower' => '3',
                'camera' => 'http://example.com',
                'pdam_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => '7',
                'ph' => '4102',
                'tds' => '2',
                'watter_lower' => '3',
                'camera' => 'http://example.com',
                'pdam_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('pdam_monitoring')->insert($data);
    }
}
