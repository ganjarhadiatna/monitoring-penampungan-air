<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PembayaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'jumlah' => '10',
                'biaya' => '5000',
                'bayar' => '50000',
                'bulan' => '06',
                'status' => '0',
                'debit_id' => '1',
                'user_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'jumlah' => '10',
                'biaya' => '5000',
                'bayar' => '50000',
                'bulan' => '06',
                'status' => '0',
                'debit_id' => '1',
                'user_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('pembayaran')->insert($data);
    }
}
